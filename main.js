import './style.css';
import * as THREE from 'three'; 
import {
	BoxGeometry,
	BufferGeometry,
	CircleGeometry,
	Color,
	ConeGeometry,
	Curve,
	CylinderGeometry,
	DodecahedronGeometry,
	DoubleSide,
	ExtrudeGeometry,
	Float32BufferAttribute,
	Group,
	IcosahedronGeometry,
	LatheGeometry,
	LineSegments,
	LineBasicMaterial,
	Mesh,
	MeshPhongMaterial,
	OctahedronGeometry,
	PerspectiveCamera,
	PlaneGeometry,
	PointLight,
	RingGeometry,
	Scene,
	Shape,
	ShapeGeometry,
	SphereGeometry,
	TetrahedronGeometry,
	TorusGeometry,
	TorusKnotGeometry,
	TubeGeometry,
	Vector2,
	Vector3,
	WireframeGeometry,
	WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { RGBELoader } from  'three/examples/jsm/loaders/RGBELoader.js';
import { InteractionManager } from 'three.interactive';

let model;

const scene = new Scene();


const camera = new PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 50 );
camera.position.z = 6;

const renderer = new WebGLRenderer( { antialias: true, canvas: document.querySelector('canvas'),alpha: true } );
renderer.setPixelRatio( window.devicePixelRatio / 1.2);
renderer.setSize( window.innerWidth, window.innerHeight );

const pmremGenerator = new THREE.PMREMGenerator( renderer );
new RGBELoader().load( 'environment.hdr', function ( texture ) {

	texture.mapping = THREE.EquirectangularReflectionMapping;

	scene.environment = texture;
    scene.background = texture;
	console.log(scene);
});

const interactionManager = new InteractionManager(
	renderer,
	camera,
	renderer.domElement
  );

const orbit = new OrbitControls( camera, renderer.domElement );

const torusgeometry = new THREE.TorusGeometry( 1.1, 0.4, 16, 100 );
const torusmaterial = new THREE.MeshStandardMaterial( { color: 'white',roughness: 0.5 } );
const radiobtn = new THREE.Mesh( torusgeometry, torusmaterial );
scene.add( radiobtn );

const spheregeometry = new THREE.SphereGeometry( 0.5, 32, 16 );
const spherematerial = new THREE.MeshStandardMaterial( { color: 'white',roughness: 0.5 } );
const sphere = new THREE.Mesh( spheregeometry, spherematerial );
scene.add( sphere );

const group = new THREE.Group();
group.add( radiobtn );
group.add( sphere );

scene.add( group );

function update(){

	
	
	group.addEventListener('mouseover', (event) =>{
	
		document.body.style.cursor = 'pointer';
   });

   group.addEventListener('mouseout', (event) =>{
	
	document.body.style.cursor = 'default';
});


group.addEventListener('click', (event) =>{
	
group.scale.set( 1.0, 1.0, 1.0);
});

group.addEventListener('mousedown', (event) =>{
	
	group.scale.set(0.9, 0.9, 0.9);
	});
		

 	};

let clicked = 1;

	interactionManager.add(group);

	group.addEventListener("click", (event) => {
     
	 if(clicked == 1){
		 
		  clicked = 0;
		  radiobtn.material.color.set('blue');
		  sphere.material.visible = true;
	 }

     else if(clicked == 0){
	
		clicked = 1;
		radiobtn.material.color.set('white')
		sphere.material.visible = false;
	 }
		
	});




function render() {

	requestAnimationFrame( render );


update()
	renderer.render( scene, camera );

}



window.addEventListener( 'resize', function () {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}, false );

render();